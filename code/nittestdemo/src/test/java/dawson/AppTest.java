package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void testEcho()
    {
        assertEquals("This test is to make sure the echo method woks as expected and returns 5 when called with the parameter 5",5, App.echo(5));
    }

    @Test
    public void testOneMore()
    {
        assertEquals("This test is to make sure the oneMore method workd as expected and returns whatever the unput parameter is plus one", 6, App.echo(5));
    }
}
